
import sys
sys.path.insert(0, '.\CustomWidgets')

from PyQt5.QtWidgets import QApplication
from Mainwindow import MainWindow

if __name__ == '__main__':
    
    app = QApplication(sys.argv)

    exc = MainWindow()
    
    sys.exit(app.exec_())
    