
import PyQt5
import cv2 as cv

import Layout

import GlobalPy

class ImagePyramids(Layout.Layout):

    def __init__(self, parent, title):
        
        super().__init__(parent, title)

        btn1 = Layout.myPushButton('3.1 Image Pyramids')
        btn1.clicked.connect(self.btn1_clicked)
        btn1.setFixedSize(self.ButtonSize)

        box = PyQt5.QtWidgets.QGridLayout()
        box.addWidget(btn1)

        self.groupBox.setLayout(box)

        self.resize(250, 120)
        self.move(305, 0)

        self.img = None
        self.width = 0
        self.height = 0
        self.qImg = None
        self.wid = None
        self.ExistWid = False
    
    def afCH(self, flag = 0):

        if flag == 1:
            self.img = cv.pyrUp(self.img, cv.BORDER_CONSTANT )

        elif flag == -1:
            self.img = cv.pyrDown(self.img, cv.BORDER_CONSTANT )
        
        self.width, self.height = self.img.shape

        self.qImg = PyQt5.QtGui.QImage(
                self.img.data,
                self.width, 
                self.height, 
                self.width, 
                PyQt5.QtGui.QImage.Format_Grayscale8
        )

        self.wid = PyQt5.QtWidgets.QWidget()
        self.wid.setFixedSize(self.width * 1.02 + 30, self.height)
        
        qlabel = PyQt5.QtWidgets.QLabel(self.wid)
        qlabel.setWindowTitle('Image Pyramids')
        qlabel.setPixmap(PyQt5.QtGui.QPixmap.fromImage(self.qImg))

        btnup = Layout.myPushButton('＋', self.wid)
        btnup.resize(25, 25)
        btnup.move(self.width * 1.02, self.height * 0.2 )
        btnup.clicked.connect(self.btnUp_clicked)

        btndown = Layout.myPushButton('－', self.wid)
        btndown.resize(25, 25)
        btndown.move(self.width * 1.02, self.height * 0.3 if self.height > 250 else self.height * 0.4)
        btndown.clicked.connect(self.btnDown_clicked)

        self.wid.show()

    @PyQt5.QtCore.pyqtSlot()
    def btn1_clicked(self):
        
        if(GlobalPy.Global_Image is not None):

            self.img = GlobalPy.Global_Image_Grey
            self.width, self.height = self.img.shape

            self.afCH()

    def btnUp_clicked(self):
        
        self.afCH(1)

    def btnDown_clicked(self):
        
        self.afCH(-1)
            
        