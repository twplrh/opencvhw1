
from PyQt5.QtWidgets import QGridLayout,  QFileDialog
from PyQt5.QtCore import pyqtSlot

import cv2
import numpy as np

import GlobalPy
import Layout

class ImageProcessing(Layout.Layout):

    def __init__(self, Parent, title = 'None'):
        
        super().__init__(Parent, title)

        Btn1 = Layout.myPushButton('1.1 Load Image File')
        Btn1.clicked.connect(self.Btn1_Clicked)
        Btn1.setFixedSize(self.ButtonSize)
        
        Btn2 = Layout.myPushButton('1.2 Color Conversion')
        Btn2.clicked.connect(self.Btn2_Clicked)
        Btn2.setFixedSize(self.ButtonSize)

        Btn3 = Layout.myPushButton('1.3 Image Flipping')
        Btn3.clicked.connect(self.Btn3_Clicked)
        Btn3.setFixedSize(self.ButtonSize)

        Btn4 = Layout.myPushButton('1.4 Blending')
        Btn4.clicked.connect(self.Btn4_Clicked)
        Btn4.setFixedSize(self.ButtonSize)

        box = QGridLayout()
        box.addWidget(Btn1)
        box.addWidget(Btn2)
        box.addWidget(Btn3)
        box.addWidget(Btn4)
        self.groupBox.setLayout(box)

        self.resize(250, 320)
        self.move(35, 0)

        self.FlipImg = None

    @pyqtSlot()
    def Btn1_Clicked(self):

        ImagePath, X = QFileDialog.getOpenFileName(self, 'Load Image File', '.', self.tr('Image (*.bmp *.png *.jpg)'))
        Filename = ImagePath.split('/')
        Filename = Filename[len(Filename)-1]

        if ImagePath == '':
            print('No Selected')
        else:
            GlobalPy.Global_Image = cv2.imread(ImagePath, cv2.IMREAD_COLOR)
            GlobalPy.Global_Image_Grey = cv2.cvtColor(GlobalPy.Global_Image, cv2.COLOR_BGR2GRAY)
            GlobalPy.ImageHeight, GlobalPy.ImageWidth, c = GlobalPy.Global_Image.shape
            cv2.imshow(Filename, GlobalPy.Global_Image)
            print("Height = {0}\nWidth = {1}".format(GlobalPy.ImageHeight, GlobalPy.ImageWidth))

    def Btn2_Clicked(self):
        
        if(GlobalPy.Global_Image is not None):

            b = GlobalPy.Global_Image[:, :, 0]
            g = GlobalPy.Global_Image[:, :, 1]
            r = GlobalPy.Global_Image[:, :, 2]

            newImage = np.dstack([g, r, b])

            cv2.imshow('Conversion', newImage)
        
        else:
            print('Load Image First！')

    def Btn3_Clicked(self):
        
        if(GlobalPy.Global_Image is not None):

            newImage = GlobalPy.Global_Image

            newImage = cv2.flip(newImage, 1)

            self.FlipImg = newImage

            cv2.imshow('Flip', newImage)
        
        else:
            print('Load Image First！')

    def Btn4_Clicked(self):
        
        if(GlobalPy.Global_Image is not None):

            if(self.FlipImg is None):
                
                print("Image Flipping First")
                
                return

            cv2.namedWindow('Blend', cv2.WINDOW_AUTOSIZE)
            cv2.createTrackbar('Alpha', 'Blend', 0, 255, self.on_change_Alpha)
            cv2.imshow('Blend', GlobalPy.Global_Image)

        else:
            print('Load Image First！')

    def on_change_Alpha(self, x):
            
        src1 = GlobalPy.Global_Image
        src2 = self.FlipImg
        dst = src2
            
        alpha = cv2.getTrackbarPos('Alpha', 'Blend')/255
        
        beta = 1.0 - alpha
            
        dst = cv2.addWeighted(src1, beta, src2, alpha, 0.0)
            
        cv2.imshow('Blend', dst)
