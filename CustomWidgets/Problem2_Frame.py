
import math

from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtCore import pyqtSlot

import cv2 as cv
import numpy as np

import Layout
import GlobalPy

class EdgeDetection(Layout.Layout):

    def __init__(self, Parent, title):
        
        super().__init__(Parent, title)

        Btn1 = Layout.myPushButton('2. Edge Detection', self)
        Btn1.clicked.connect(self.Btn1_Clicked)
        Btn1.setFixedSize(self.ButtonSize)

        box = QGridLayout()
        box.addWidget(Btn1)

        self.groupBox.setLayout(box)

        self.resize(250, 120)

        self.move(35, 300)

        self.GreyScaleImage = None

    @pyqtSlot()
    def Btn1_Clicked(self):
        
        if(GlobalPy.Global_Image is not None):

            self.GreyScaleImage = cv.cvtColor(GlobalPy.Global_Image, cv.COLOR_BGR2GRAY)

            Smoothing = cv.GaussianBlur(self.GreyScaleImage, (3,3), 0)

            nMin = Smoothing.min()
            nMax = Smoothing.max()
            blank_image = np.zeros((GlobalPy.ImageWidth, GlobalPy.ImageHeight), np.uint8)

            for x in range(GlobalPy.ImageWidth):
                for y in range(GlobalPy.ImageHeight):
                    Val = ((Smoothing[x][y] - nMin)*255 / (nMax - nMin))
                    if(Val > 40):
                        blank_image[x][y] = Val

#            x = mySobel().Shelter(blank_image)

            cv.namedWindow('Sobel')
            cv.createTrackbar('Mag', 'Sobel', 0, 255, lambda x : x)
            cv.setTrackbarPos('Mag', 'Sobel', 40)
            cv.imshow('Sobel', blank_image)
            
        else:
            print('Load Image First！')

class mySobel():
    
    def __init__(self):
        
        super().__init__()

        self.xorder = [[-1, 0, 1] , [-2, 0, 2], [-1, 0, 1]]
        
        self.yorder = [[-1, -2, -1], [ 0,  0,  0], [ 1,  2,  1]]

    def Shelter(self, src):
        
        blank_image = np.zeros((GlobalPy.ImageWidth, GlobalPy.ImageHeight), np.uint8)
        temp_image = src

        if(GlobalPy.ImageHeight > GlobalPy.ImageWidth):
            GlobalPy.ImageHeight = GlobalPy.ImageWidth
        else:
            GlobalPy.ImageWidth = GlobalPy.ImageHeight

        for x in range(GlobalPy.ImageHeight):
            for y in range(GlobalPy.ImageWidth):
                print(temp_image[x][y])

        for x in range(1, GlobalPy.ImageHeight-1):
            for y in range(1, GlobalPy.ImageWidth-1):
                    
                G_x = ( 
                    self.xorder[0][0] * src[x-1][y-1] + self.xorder[0][1] * src[x][y-1] + self.xorder[0][2] * src[x+1][y-1]
                  + self.xorder[1][0] * src[x-1][y] + self.xorder[1][1] * src[x][y] + self.xorder[1][2] * src[x+1][y]
                  + self.xorder[2][0] * src[x-1][y+1] + self.xorder[2][1] * src[x][y+1] + self.xorder[2][2] * src[x+1][y+1] 
                )

                G_y = ( 
                    self.yorder[0][0] * src[x-1][y-1] + self.yorder[0][1] * src[x][y-1] + self.yorder[0][2] * src[x+1][y-1]
                  + self.yorder[1][0] * src[x-1][y] + self.yorder[1][1] * src[x][y] + self.yorder[1][2] * src[x+1][y]
                  + self.yorder[2][0] * src[x-1][y+1] + self.yorder[2][1] * src[x][y+1] + self.yorder[2][2] * src[x+1][y+1] 
                )

                val = math.sqrt(G_x * G_x + G_y * G_y)

                blank_image[x][y] = val

        return blank_image

