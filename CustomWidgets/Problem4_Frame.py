

import PyQt5
import cv2

import Layout
import GlobalPy

class AdaptiveThreshold(Layout.Layout):

    def __init__(self, parent, title):
        
        super().__init__(parent, title)

        btn1 = Layout.myPushButton('4.1 Global Threshold')
        btn1.clicked.connect(self.btn1_clicked)
        btn1.setFixedSize(self.ButtonSize)

        btn2 = Layout.myPushButton('4.2 Local Threshold')
        btn2.clicked.connect(self.btn2_clicked)
        btn2.setFixedSize(self.ButtonSize)

        box = PyQt5.QtWidgets.QGridLayout()
        box.addWidget(btn1)
        box.addWidget(btn2)

        self.groupBox.setLayout(box)

        self.resize(250, 250)
        self.move(305, 110)
 
 
    @PyQt5.QtCore.pyqtSlot()
    def btn1_clicked(self):
        ret, img = cv2.threshold(GlobalPy.Global_Image_Grey, 80, 255, cv2.THRESH_BINARY )
        cv2.imshow("Global Threshold", img)

    def btn2_clicked(self):
        img = cv2.adaptiveThreshold(GlobalPy.Global_Image_Grey, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 19, -1)
        cv2.imshow("Local Threshold", img)
