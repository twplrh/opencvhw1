
import PyQt5
from PyQt5.QtWidgets import QScrollBar, QLabel, QTextEdit
from PyQt5.QtCore import *

import cv2
import numpy as np

import Layout
import GlobalPy

class ImageTransformation(Layout.Layout):

    def __init__(self, parent, title):
        
        super().__init__(parent, title)

        area = LayoutUnderP5(self, '5.1 Rotation, Scaling, Translation')

        btn1 = Layout.myPushButton('5.2 Perspective Transform')
        btn1.clicked.connect(self.btn1_clicked)
        btn1.setFixedSize(self.ButtonSize)

        box = PyQt5.QtWidgets.QGridLayout()
        box.addWidget(area,0,0)
        box.addWidget(btn1,1,0, Qt.AlignCenter)

        self.groupBox.setLayout(box)
                
        self.resize(250, 400)
        self.move(575, 0)

    @PyQt5.QtCore.pyqtSlot()
    def btn1_clicked(self):
        pass

class LayoutUnderP5(Layout.Layout):

    def __init__(self, parent, title):
        
        super().__init__(parent, title)

        self.x1 = self.CreateQTextEdit(70, 22, 'Angle (deg)', self)
        self.x2 = self.CreateQTextEdit(70, 22, 'Scale', self)
        self.x3 = self.CreateQTextEdit(70, 22, 'Tx (Pixel)', self)
        self.x4 = self.CreateQTextEdit(70, 22, 'Ty (Pixel)', self)

        btn1 = Layout.myPushButton('5.1 Rotation, Scaling, Translation')
        btn1.clicked.connect(self.btn1_clicked)
        btn1.setFixedSize(160, 50)

        btn1.setFont(PyQt5.QtGui.QFont('abc', 8) )

        box = PyQt5.QtWidgets.QGridLayout()
        box.addWidget(self.x1, 0, 0)
        box.addWidget(self.x2, 0, 1)
        box.addWidget(self.x3, 1, 0)
        box.addWidget(self.x4, 1, 1)
        box.addWidget(btn1)

        self.groupBox.setLayout(box)

        self.setFixedSize(200, 250)

        self.Tx = 130
        self.Ty = 125
        self.src = None

    def CreateQTextEdit(self, width, height, text, Papa):
        
        x = QTextEdit('', Papa)
        x.setFixedSize(width, height)
        x.setPlaceholderText(text)
        x.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        x.setTabChangesFocus(True)
        
        return x

    @PyQt5.QtCore.pyqtSlot()
    def btn1_clicked(self):

        if GlobalPy.Global_Image is not None:
            
            if self.src is None:
                self.src = GlobalPy.Global_Image

            try :
                Angle = float(self.x1.toPlainText())
            except ValueError:
                Angle = 0

            try :
                Scale = float(self.x2.toPlainText())
            except ValueError:
                Scale = 0
        
            try :
                Tx = float(self.x3.toPlainText())     
            except ValueError:
                Tx = 0
        
            try :
                Ty = float(self.x4.toPlainText())
            except ValueError:
                Ty = 0

            M_Ro = cv2.getRotationMatrix2D((self.Tx, self.Ty), Angle, Scale)
            M_Tr = np.float32([[1,0,Tx], [0,1,Ty]])

            self.Tx = self.Tx + Tx
            self.Ty = self.Ty + Ty

            temp = cv2.warpAffine(self.src, M_Ro, (GlobalPy.ImageWidth, GlobalPy.ImageHeight))
            dst = cv2.warpAffine(temp, M_Tr, (GlobalPy.ImageWidth, GlobalPy.ImageHeight))

            cv2.imshow('Rotation, Scaling, Translation', dst)

            self.src = dst

