
import PyQt5

class Layout(PyQt5.QtWidgets.QWidget):

    def __init__(self, parent, title):
        
        super().__init__(parent)

        self.groupBox = self.CreateGroupBox(title)
        self.ButtonSize = PyQt5.QtCore.QSize(190, 50)

        grid = PyQt5.QtWidgets.QGridLayout(self)
        grid.addWidget(self.groupBox)

    def CreateGroupBox(self, title):
        
        groupBox = PyQt5.QtWidgets.QGroupBox(title)

        groupBox.setStyleSheet("""
            QGroupBox {
                border: 1px solid gray;
                border-color: #FF17365D;
                margin-top: 27px;
                font-size: 12px;
                border-top-left-radius: 12px;
                border-top-right-radius: 12px;
                border-bottom-left-radius: 12px;
                border-bottom-right-radius: 12px;
                background-color: qlineargradient(x1:0, y1:0, x2:0.2, y2:0.8, stop:0 #ffffff, stop:1 #edf7ff);
            }

            QGroupBox::title {
                top: -5px;
                left: 10px;
            }
            """)

        return groupBox

class myPushButton(PyQt5.QtWidgets.QPushButton):

    def __init__(self, str, parent = None):
        
        super().__init__(str, parent)

        self.setStyleSheet("""
            QPushButton { 
                border: 1px solid gray;
                border-color: #FF17365D;
                border-radius: 6px;
            }
            QPushButton:hover{ background-color: gray; }
            QPushButton:pressed{ background-color: #b2cfff; }
        """)