
import ctypes

import PyQt5

from PyQt5.QtWidgets import QWidget

from Problem1_Frame import ImageProcessing
from Problem2_Frame import EdgeDetection
from Problem3_Frame import ImagePyramids
from Problem4_Frame import AdaptiveThreshold
from Problem5_Frame import ImageTransformation

class MainWindow(QWidget):

    def __init__(self):
        
        super().__init__()

        self.title = 'openCV_HW1'
        self.width = 860
        self.height = 440
        self.Image = None

        self.initUI()

    def __del__(self):

        print("Destroy Everything.")

    def initUI(self):

        screenSize = ctypes.windll.user32
        screenSize.SetProcessDPIAware()
        [w, h] = [screenSize.GetSystemMetrics(0), screenSize.GetSystemMetrics(1)]

        self.setWindowTitle(self.title)
        self.setGeometry(w/4, h/4, 0, 0)
        self.setFixedHeight(self.height)
        self.setFixedWidth(self.width)

        self.setStyleSheet("QWidget{ background-color : #f4f8ff;}")

        ImageProcessing(self, '1. Image Processing')
        EdgeDetection(self, '2. Edge Detection')
        ImagePyramids(self, '3. Image Pyramids')
        AdaptiveThreshold(self, '4. Adaptive Threshold')
        ImageTransformation(self, '5. Image Transformation')

        self.show()
